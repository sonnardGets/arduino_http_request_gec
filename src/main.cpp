/*
 * 
 *  Petit soft permetant à un arduino de faire un request sur les modulesIP
 *  L'arduino renverra sur son uart le resultat du test
 *  <TCPOK>  cr
 *  <TCPKO>  cr
 * 
 *  Nicolas Sonnard 
 *  Ecrit en 2019
 *  
 */

// Ces deux bibliothèques sont indispensables pour le shield
#include <SPI.h>
#include <Ethernet.h>

// L'adresse MAC du shield
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0E, 0xA5, 0x7E };
// L'adresse IP que prendra le shield
IPAddress ip(192, 168, 2, 253);
// L'objet qui nous servira à la communication
EthernetClient client;
// Le serveur à interroger
//IPAddress server(192,168,2,225);
char server[] = "192.168.2.225";

char InputBufer[50];
int J;
int LenBuffer = 0;
bool stringComplete = false;  // whether the string is complete
String TxtRequest = "";

void json_send_error(char* error);
void json_send_terminal(char *txt);
//prototypes des fonctions
void request_reset(void);



void setup() {



  // On démarre la voie série pour déboguer
  //Serial.begin(115200);
  Serial1.begin(115200);
    Serial.begin(115200);



  Ethernet.begin(mac, ip);

  //Serial.println("Init...");
  // Donne une seconde au shield pour s'initialiser
  delay(1000);
  json_send_terminal("Ip testeur ready");
  //request_reset();
    // Si on est bien déconnecté.
  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    //Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    //Serial.println("Ethernet cable is not connected.");
  }
}
void loop()
{
  //request();
  char carlu = 0;

  // renvoie le resultat du request au pc
  while (client.available()) {
    carlu = client.read();
    //Serial.print(carlu);
  }




  while (Serial1.available()) {

    char inChar = Serial1.read();
    //Serial.println("receive data");
    //Serial.println(inChar);

    if (inChar == '(') J = 0;
    InputBufer[J] = inChar;


  

    J++;


    // test si la fin de la chaine est vue.
    if (inChar == ')') {
      stringComplete = true;
      J = 0;
      // regarde si la chaine correspond Ã  une instruction CAN du PC

    }



  }


  // on test si le premier caractÃ¨re est "(" sinon tous est ignorÃ© et on reset
  if (InputBufer[0] == '(') {

    if (stringComplete == true) {

      if (InputBufer[1] == 'G' && InputBufer[2] == 'E' && InputBufer[3] == 'T' )
      {

        LenBuffer = strlen(InputBufer);

        // envoie les data à l'uart 3 (bypasse sur arduino 2)
        for (int K = 4 ; K < LenBuffer - 1; K++)
        {
          TxtRequest = TxtRequest + (InputBufer[K]);
        }
        //Serial.print(TxtRequest);

      }

      // commande (RESET;
      if (InputBufer[1] == 'R' && InputBufer[2] == 'E' && InputBufer[3] == 'S' && InputBufer[4] == 'E' && InputBufer[5] == 'T' )
      {
        json_send_terminal("Reset ip de la platine !");
        Serial.println("Reset tcp" );
        request_reset();
      }


      stringComplete = false;
      InputBufer[0] = 0;

    }
    
  }

  else
  {
    InputBufer[0] = 0;
  }





}

void request_reset(void)
{


  int erreur = 0;
  if (client.connect(server, 80)) {
    // Pas d'erreur ? on continue !
    Serial.println("Connexion OK, envoi en cours...");
    json_send_terminal("Connexion OK, envoi en cours...");


    /*
        selon wireshark venant du python
        GET / HTTP/1.1\r\n
          Request Method: GET
          Request URI: /
          Request Version: HTTP/1.1
        Host: 192.168.2.225\r\n
        Accept-Encoding: identity\r\n
        Authorization: Basic YWRtaW46MjQ5NQ==\r\n
        \r\n
    */

    // ici on met tous sur une ligne sinon le webserver keil n'accepte pas la requête
    client.print("GET /general.cgi?factory_config= HTTP/1.1\r\nHost: 192.168.2.225\r\nAuthorization: Basic YWRtaW46MjQ5NQ==\r\n\r\n");

    //json_send_error( "Reset et tests IP OK");
    json_send_terminal("Reset et tests IP OK");
  


  } 
  else {


    //json_send_error( "Erreur du test IP");
    json_send_terminal("Erreur du test IP");
    //envoie au GUI de l'erreure reseau sous forme de fennettre windows
    Serial1.print("{\"typemsg\":\"GUI\",\"warning_box\":\"Erreur du test reseau verifier le cable reseau et refaite le test si besoin\"}");
    Serial1.println();



  }

}


/*

    envoie d'une erreur sous form JSON

    ("{\"typemsg\":\"communication\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void json_send_error( char *error)
{
  Serial1.print("{\"typemsg\":\"GUI\"");
  Serial1.print(",\"Error\":{\"type\":\"");
  Serial1.print(error);
  Serial1.println("\"}}");
}


/*

    envoie d'un message au terminal du gui sous form JSON

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void json_send_terminal(char *txt)
{
  Serial1.print("{\"typemsg\":\"GUI\",\"terminal\":{\"txt\":\"");
  Serial1.print(txt);
  Serial1.println("\"}}");
}



